const gulp = require('gulp');

// Enable or disable development
// ==============================================

let isDev = process.argv.includes('--dev');

// ==============================================

// BrowserSync task
// ==============================================
const browserSync = require('browser-sync').create();

gulp.task('browserSync', () => {
  browserSync.init({ server: './dist' });
});
// ==============================================

// JavaScript
// ==============================================

gulp.task('js', () => {
  // Task dependencies
  const sourcemaps = require('gulp-sourcemaps');
  const plumber = require('gulp-plumber');
  const ifElse = require('gulp-if-else');

  const babel = require('gulp-babel');
  const babelCore = require('babel-core');
  const babelPresetEnv = require('babel-preset-env');

  const concat = require('gulp-concat');
  const uglify = require('gulp-uglify');

  gulp
    .src('src/js/**/*.js')
    .pipe(plumber())
    .pipe(ifElse(isDev, () => sourcemaps.init()))
    .pipe(babel({ presets: ['env'] }))
    .pipe(concat('main.js'))
    .pipe(ifElse(isDev, () => sourcemaps.write()))
    .pipe(ifElse(!isDev, () => uglify()))
    .pipe(gulp.dest('dist'))
    .pipe(browserSync.stream());
});
// ==============================================

// CSS / SCSS
// ==============================================

gulp.task('scss', () => {
  // Task dependencies
  const sourcemaps = require('gulp-sourcemaps');
  const plumber = require('gulp-plumber');
  const ifElse = require('gulp-if-else');

  const sass = require('gulp-sass');
  const autoprefixer = require('autoprefixer');
  const cleanCSS = require('gulp-clean-css');
  const postcss = require('gulp-postcss');

  gulp
    .src('src/scss/main.scss')
    .pipe(plumber())
    .pipe(ifElse(isDev, () => sourcemaps.init()))
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([autoprefixer()]))
    .pipe(ifElse(isDev, () => sourcemaps.write()))
    .pipe(ifElse(!isDev, () => cleanCSS()))
    .pipe(gulp.dest('dist'))
    .pipe(browserSync.stream());
});
// ==============================================

// Prettier
// ==============================================
gulp.task('prettierScss', () => {
  // Task dependencies
  const prettier = require('gulp-prettier');

  gulp
    .src(['src/scss/**/*.scss'])
    .pipe(prettier())
    .pipe(gulp.dest(file => file.base));
});

gulp.task('prettierJs', () => {
  // Task dependencies
  const prettier = require('gulp-prettier');

  gulp
    .src(['src/js/**/*.js', 'gulpfile.js', '!src/js/lib/**/*.js'])
    .pipe(prettier({ singleQuote: true }))
    .pipe(gulp.dest(file => file.base));
});
// ==============================================

// Images task
// ==============================================

gulp.task('images', () => {
  // Task dependencies
  const flatten = require('gulp-flatten');

  gulp
    .src('src/images/**/*')
    .pipe(flatten())
    .pipe(gulp.dest('dist'))
    .pipe(browserSync.stream());
});
// ==============================================

// html task
// ==============================================

gulp.task('html', () => {
  // Task dependencies
  const flatten = require('gulp-flatten');

  gulp
    .src('src/html/**/*.html')
    .pipe(flatten())
    .pipe(gulp.dest('dist'))
    .pipe(browserSync.stream());
});
// ==============================================

// Main tasks
// ==============================================
// Commands for production:
// 1. gulp build -> building files for production
// 2. gulp watch -> build and watch files for production
//
// Commands for development
// 1. gulp build -> building files for development
// 2. gulp watch -> build and watch files for development
// ==============================================

gulp.task('default', () => {
  // Task dependencies
  const runSequence = require('run-sequence');
  const del = require('del');

  del(['dist']).then(() => runSequence('html', 'js', 'sass', 'images'));
});

gulp.task('watch', ['default', 'browserSync'], () => {
  gulp.watch('src/html/**/*.html', ['html']);
  gulp.watch('src/scss/**/*.scss', ['sass', 'prettierScss']);
  gulp.watch('src/js/**/*.js', ['js', 'prettierJs']);
  gulp.watch('src/images/**/*', ['images']);
});
// ==============================================

// ==============================================
// That is all, bye
// ==============================================
